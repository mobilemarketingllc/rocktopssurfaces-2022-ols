<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12 product_single_countertops">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<div class="product-detail-layout-6 contrtops-temp">
				<?php
				global $post;
				$flooringtype = get_post_type();
				$meta_values = get_post_meta( get_the_ID() );
				$brand = $meta_values['brand'][0] ;
				$color = $meta_values['color'][0] ;
				$flag = $meta_values['flag'][0];
				$org_image = $meta_values['product_image'][0];
				if(strpos($org_image , 'http') === false){ 
					$org_image = "https://" . $org_image;
				}	
				$image = "https://mm-media-res.cloudinary.com/image/fetch/h_800,w_800,c_limit/".$org_image."";
				$toggle_image = "https://mm-media-res.cloudinary.com/image/fetch/h_500,w_500,c_limit/".$org_image."";
				$product = $meta_values['product'][0] ;
				$type = $meta_values['type'][0];
				$product_id = get_the_ID();
				$familycolor = $meta_values['brand'][0];
				$key = 'brand';

				$args = array(
					'post_type'      => $flooringtype,
					'posts_per_page' => -1,
					'post_status'    => 'publish',
					'meta_query'     => array(
						array(
							'key'     => $key,
							'value'   => $familycolor,
							'compare' => '='
						),
						array(
							'key' => 'product_image',
							'value' => '',
							'compare' => '!='
							)
					)
				);										

				$the_query = new WP_Query( $args );
					
				?>
				<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
					<div class="fl-post-content clearfix grey-back" itemprop="text">
						<div class="clearfix"></div>
							<div class="row">
								<div class="col-md-6 col-sm-12 product-swatch">   
									<div class="imagesHolder">
										<div id="product-images-holder" >
											<?php  if (!empty($image)){ ?>
												<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="<?php echo $image; ?>" data-src="<?php echo $image; ?>" data-exthumbimage="<?php echo $image; ?>">
													<a href="javascript:void(0)" class="popup-overlay-link"></a>
													<span class="main-imgs"><img src="<?php echo $toggle_image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
												</div>
											<?php } else{ ?>
												<div class="img-responsive toggle-image" data-targetimg="gallery_item_0" style="background-image:url('http://placehold.it/168x123?text=COMING+SOON');background-size: cover;background-position:center">
													<a href="http://placehold.it/168x123?text=COMING+SOON" class="popup-overlay-link"></a>
													<span class="main-imgs"><img src="http://placehold.it/168x123?text=COMING+SOON" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
												</div>
											<?php } ?>
        
										
    									</div>
            
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-md-5 col-sm-12 product-box">
									<div class="row">
										<div class="col-md-6">
											
											<?php if(array_key_exists("brand",$meta_values)){?>
												<h1 class="fl-post-title" itemprop="name"><?php echo $meta_values['brand'][0]; ?></h1>
											<?php } ?>

											<?php if(array_key_exists("color",$meta_values)){ ?>
												<h2 class="fl-post-title" itemprop="name"><?php  echo $meta_values['color'][0]; ?></h2>
											<?php } ?>
												<?php if(isset($meta_values['flag'][0]) ){ 
												if(strtolower(trim($meta_values['flag'][0])) == "in stock"){ ?>
													<div class="countertop_flag flag_in_stock">
														<p>In Stock</p>
													</div>
												<?php }else if(strtolower(trim($meta_values['flag'][0])) == "special order"){ ?>
													<div class="countertop_flag flag_special_order">
														<p>Special Order</p>
													</div>
												<?php } 
											}    ?>

										</div>							
										<div class=" col-md-6 text-right">
										
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="row">
										<div class="col-md-12">	
											<!-- <div class="product-colors">
												<ul>
													<li class="found"><?php echo $the_query ->found_posts; ?></li>
													<li class="colors">Colors Available</li>
												</ul>
											</div> -->

											<!-- <div id="product-colors">
												<div class="product-variations">
													<div class="color_variations_slider_1">
														<div class="slides">
															<?php
															while ($the_query->have_posts()) {
															$the_query->the_post();
															$image = get_field('product_image');
															if(strpos($image , 'http') === false){ 
																$image = "https://" . $image;
															}	
															$image = "https://mm-media-res.cloudinary.com/image/fetch/h_222,w_222,c_limit/".$image."";
															$style = "padding: 5px;";
															?>
																<div class="slide col-md-2 col-sm-3 col-xs-6 color-box <?php if($product_id == get_the_ID()){ echo 'selected-slide';}?>">
																	<figure class="color-boxs-inner">
																		<div class="color-boxs-inners">
																			<a href="<?php the_permalink(); ?>">
																				<img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
																			</a>
																			
																			<div><small><?php the_field('color'); ?></small></div>
																		</div>
																	</figure>
																</div>
															<?php } ?>
														</div>
													</div>
												</div>
												<?php wp_reset_postdata(); ?> 
											</div> -->
										</div>	
									</div>		


									
									<div class="button-wrapper">
										<!-- <div class="dual-button">
											<a href="/flooring-financing/" class="button contact-btn">FINANCING</a>	
										</div> -->
										<!-- <a href="/request-a-quote/" class="button contact-btn">REQUEST A QUOTE</a> -->
										<h2 class="hidden countertop_product_title"><?php echo get_the_title();?></h2>
										<a class="button contact-btn countertop-request-form-open" href="javascript:void(0)">REQUEST A QUOTE</a>
										<?php if(strtolower(trim($meta_values['flag'][0])) != "special order stone"){ ?>
										<a href="/in-stock-countertop-sample-order/?product_id=<?php echo get_the_id();?>" class="button contact-btn">Order your free sample</a> 
										<?php } ?>
									</div>						
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="product-attributes-wrap">
										<div class="product-attributes">
											<h3>Product Attributes</h3>
											<table class="table">
												<tbody>
													<?php if(array_key_exists("brand",$meta_values) && $meta_values['brand'][0]!=''){ ?>
														<tr>
															<th scope="row">Brand</th>
															<td><?php echo $meta_values['brand'][0]; ?></td>
														</tr>
													<?php } ?>
													<?php if(array_key_exists("color",$meta_values) && $meta_values['color'][0]!=''){  ?>
														<tr>
															<th scope="row">Color</th>
															<td><?php echo $meta_values['color'][0]; ?></td>
														</tr>
													<?php } ?>
													<?php if(array_key_exists("type",$meta_values) && $meta_values['type'][0]!=''){  ?>
														<tr>
															<th scope="row">Type</th>
															<td><?php echo $meta_values['type'][0]; ?></td>
														</tr>
													<?php } ?>
													<!-- <?php if(array_key_exists("product",$meta_values) && $meta_values['product'][0]!=''){  ?>
														<tr>
															<th scope="row">Product</th>
															<td><?php echo $meta_values['product'][0]; ?></td>
														</tr>
													<?php } ?> -->
												</tbody>
											</table>
										</div>


									</div>		
								</div>
							</div>		
							<div class="clearfix"></div>
						</div>
					</article>
				</div>
				<?php
				$title = get_the_title();
				$last_date = date('Y') . '-12-31';

				$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','aggregateRating'=>array('@type'=>'AggregateRating','ratingValue'=>'4.4','reviewCount'=>'89'),'name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'Brand','name'=>$brand), 
				'offers'=>array('@type'=>'offer','url'=> home_url(),'priceCurrency'=>'USD','price'=>'00', 'availability' => 'InStock','priceValidUntil'=>$last_date),'review'=>array('@type'=>'Review','reviewRating'=>array('@type'=>'Rating','ratingValue'=>'4','bestRating'=>'5'),'author'=>array('@type'=>'Organization','name'=>get_bloginfo())));
				?>
				<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>	

			<?php endwhile; endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>