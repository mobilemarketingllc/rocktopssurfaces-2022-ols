<div class="product-plp-grid product-grid swatch product-countertop" itemscope itemtype="http://schema.org/ItemList">
    <div class="row product-row">
        <?php 
        global $wpdb;
        $show_financing = get_option('sh_get_finance');
        $col_class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12';
        $salebrand = get_option('salesbrand');
        if($salebrand !=''){
            $slide_brands = rtrim($salebrand, ",");
            $brandonsale = array_filter(explode(",",$slide_brands));
            $brandonsale = array_map('trim', $brandonsale);
        }
        $K = 1;

        $getcouponbtn = get_option('getcouponbtn');
        $getcouponreplace = get_option('getcouponreplace');
        $getcouponreplacetext = get_option('getcouponreplacetext');
        $getcouponreplaceurl = get_option('getcouponreplaceurl');
        $pdp_get_finance = get_option('pdp_get_finance');
        $getfinancereplace = get_option('getfinancereplace');
        $getfinancereplaceurl = get_option('getfinancereplaceurl');
        $getfinancetext = get_option('getfinancetext');
        $getcoupon_link = get_option('getcoupon_link');

        while ( $prod_list->have_posts() ): $prod_list->the_post(); 
        //collection field
        $meta_values = get_post_meta( get_the_ID() );  
        $collection = $meta_values['collection'][0];
        $brand =  $meta_values['brand'][0];      
        $flooringtype = get_post_type();
        $product_url = get_the_permalink();
        ?>
        <div class="<?php echo $col_class; ?>">    
            <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

                <meta itemprop="position" content="<?php echo $K; echo $prod_list->ID;?>" />
                <?php if($meta_values['product_image'][0]) { ?>
                    <div class="fl-post-grid-image">
                        <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php 
                            $image = $meta_values['product_image'][0]; 
                            if(strpos($image , 'http') === false){ 
                                $image = "https://" . $image;
                            }	
                            $image = "https://mm-media-res.cloudinary.com/image/fetch/h_222,w_222,c_limit/".$image."";
                            ?>
                            <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                        </a>
						<?php if(isset($meta_values['flag'][0]) ){ 
                                if(strtolower(trim($meta_values['flag'][0])) == "in stock"){ ?>
                                    <div class="countertop_flag flag_in_stock">
                                        <p>In Stock</p>
                                    </div>
                                <?php }else if(strtolower(trim($meta_values['flag'][0])) == "special order"){ ?>
                                    <div class="countertop_flag flag_special_order">
                                        <p>Special Order</p>
                                    </div>
                                <?php } 
                            }    ?>
                    </div>
                <?php } else { ?>
                    <div class="fl-post-grid-image">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php //the_post_thumbnail($settings->image_size); ?>
                            <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                        </a>                
                    </div>

                <?php } ?>
                <div class="fl-post-grid-text product-grid btn-grey">
                    <h4><span class="coretec_text"> <?php  echo $meta_values['brand'][0];  ?></span>
                        <span class="style_text"><?php echo $meta_values['product'][0]; ?> </span> 
                    </h4>    
                </div>

                <a class="fl-button plp_box_btn countertop_plp_box_btn" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
                <a class="fl-button plp_box_btn countertop_plp_box_btn countertop-request-form-open" href="javascript:void(0)">Request a Quote</a>
                <?php  if(trim($meta_values['flag'][0]) == "IN-STOCK"){ ?>   <a class="fl-button plp_box_btn countertop_plp_box_btn" href="/in-stock-countertop-sample-order/?product_id=<?php echo get_the_ID(); ?>">Order Free Sample</a><?php } ?>
                <h2 class="hidden countertop_product_title"><?php echo get_the_title();?></h2>
            </div>
        </div>

        <?php  $K++;  endwhile; wp_reset_postdata();?>
    </div>
</div>