<?php

add_action( 'init', 'register_bb_cron_delete_event');

// Function which will register the event
function register_bb_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_brand_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_brand_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_hook_activation');

/**  Function for activate cron job  **/
function bb_hook_activation() {
    if (! wp_next_scheduled ( 'bb_brand_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_brand_sync');
    }
}


//add_action( 'bb_brand_sync', 'bb_brand_function' );


function bb_brand_function() {
    
    $product_json =  json_decode(get_option('product_json'));

    if(is_array($product_json) && count($product_json) > 0){
       
        $vendor_page = get_page_by_path( '/our-vendors/', OBJECT, 'page' );
        
        $parent = new WP_Query(array('post_type' =>'page','post_parent' => $vendor_page->ID ,'post_per_page'=>-1));


        if ($parent->have_posts()) : ?>

            <?php while ($parent->have_posts()) : $parent->the_post(); 
                $post = array( 'ID' => get_the_ID(), 'post_status' => "draft" );
                wp_update_post($post);
            endwhile;
            unset($parent); 
        endif; 
        wp_reset_postdata();
        $brand_pg = array();
        foreach($product_json as $product_detail){
            if(isset($product_detail->manufacturer)){
                if(count($brand_pg) == 0 || !in_array(trim($product_detail->manufacturer) , $brand_pg)){
                    $check_brand_lp = get_page_by_title( trim($product_detail->manufacturer) );
                    if(isset($check_brand_lp) && get_post_status($check_brand_lp->ID) != "publish"){
                        $post = array( 'ID' => $check_brand_lp->ID, 'post_status' => "publish" );
                        wp_update_post($post); 
                        $brand_pg[]= trim($product_detail->manufacturer);
                    }
                }
            }
        }
    } 
}

// Vendor menu list
function bb_brand_vendor_items() {
    ob_start();
    $vendor_page = get_page_by_path( '/our-vendors/', OBJECT, 'page' );
    ?> 
    <ul class="vendor_brand_menu_list">
    <?php
        wp_list_pages( array(
            'title_li'    => '',
            'child_of'    => $vendor_page->ID,
            'show_date'   => ''
        ) );
    ?>
    </ul>
    <?php
    return ob_get_clean();
}
add_shortcode( 'BRAND_VENDOR_ITEMS', 'bb_brand_vendor_items' );

//CDE location Sync process


add_action( 'init', 'register_bbsync_cron_delete_event');

// Function which will register the event
function register_bbsync_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_location_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_location_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_sync_hook_activation');

/**  Function for activate cron job  **/
function bb_sync_hook_activation() {
    if (! wp_next_scheduled ( 'bb_location_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_location_sync');
    }
}


//add_action( 'bb_location_sync', 'bb_location_sync_function' );

function bb_location_sync_function() {

    global $wpdb;

    if ( ! function_exists( 'post_exists' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/post.php' );
    }

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

     
        for($i=0;$i<count($website['result']['locations']);$i++){

            if($website['result']['locations'][$i]['type'] == 'store'){     
                
                
                $location = $apiObj->call(BASEURL.$website['result']['locations'][$i]['clientCode'],"GET",$inputs,$headers);           

               
                $location_url = preg_replace('#^https?://#', '', rtrim($website['result']['locations'][$i]['url'],'/')); 

                $location_rugcode = $location['result']['rugAffiliateCode'];   

                $location_name = $website['result']['locations'][$i]['name'] ;             

                $found_post = post_exists($location_name,'','','wpsl_stores');

                $store_hours = array();

                              $store_hours = array(
                                'monday'=> array(str_replace('-',',',$website['result']['locations'][$i]['monday'])),
                                'tuesday'=>array(str_replace('-',',',$website['result']['locations'][$i]['tuesday'])),
                                'wednesday'=>array(str_replace('-',',',$website['result']['locations'][$i]['wednesday'])),
                               'thursday'=>array(str_replace('-',',',$website['result']['locations'][$i]['thursday'])),
                                'friday'=>array(str_replace('-',',',$website['result']['locations'][$i]['friday'])),
                                'saturday'=>array(str_replace('-',',',$website['result']['locations'][$i]['saturday'])),
                                'sunday'=>array(str_replace('-',',',$website['result']['locations'][$i]['sunday']))
                                             
                              );

                $store_hours_json = serialize($store_hours);
             

                     if( $found_post == 0 ){

                            $array = array(
                                'post_title' => $location_name,
                                'post_type' => 'wpsl_stores',
                                'post_content'  => "",
                                'post_status'   => 'publish',
                                'post_author'   => 0,
                            );
                            $post_id = wp_insert_post( $array );                        
                            
                            update_post_meta($post_id, 'wpsl_address', $website['result']['locations'][$i]['address']); 
                            update_post_meta($post_id, 'wpsl_city', $website['result']['locations'][$i]['city']); 
                            update_post_meta($post_id, 'wpsl_state', $website['result']['locations'][$i]['state']); 
                            update_post_meta($post_id, 'wpsl_country', $website['result']['locations'][$i]['country']); 
                            update_post_meta($post_id, 'wpsl_zip', $website['result']['locations'][$i]['postalCode']); 
                            update_post_meta($post_id, 'wpsl_store_clientcode', $website['result']['locations'][$i]['clientCode']); 
                            update_post_meta($post_id, 'wpsl_rugshop_code', $location_rugcode); 

                            if($website['result']['locations'][$i]['forwardingPhone']==''){

                              update_post_meta($post_id, 'wpsl_phone', $website['result']['locations'][$i]['phone']);  
                              
                            }else{

                              update_post_meta($post_id, 'wpsl_phone', $website['result']['locations'][$i]['forwardingPhone']);  
                            }
                                                               
                            update_post_meta($post_id, 'wpsl_lat', $website['result']['locations'][$i]['lat']); 
                            update_post_meta($post_id, 'wpsl_lng', $website['result']['locations'][$i]['lng']); 
                            update_post_meta($post_id, 'wpsl_store_shortname', $location_name); 
                            update_post_meta($post_id, 'wpsl_site_url', $location_url); 
                            update_post_meta($post_id, 'wpsl_hours', $store_hours_json);
                            
                            
                    }else{

                              update_post_meta($found_post, 'wpsl_address', $website['result']['locations'][$i]['address']); 
                              update_post_meta($found_post, 'wpsl_city', $website['result']['locations'][$i]['city']); 
                              update_post_meta($found_post, 'wpsl_state', $website['result']['locations'][$i]['state']); 
                              update_post_meta($found_post, 'wpsl_country', $website['result']['locations'][$i]['country']); 
                              update_post_meta($found_post, 'wpsl_zip', $website['result']['locations'][$i]['postalCode']); 
                              update_post_meta($found_post, 'wpsl_store_clientcode', $website['result']['locations'][$i]['clientCode']); 
                              update_post_meta($found_post, 'wpsl_rugshop_code', $location_rugcode); 
                              if($website['result']['locations'][$i]['forwardingPhone']==''){

                              update_post_meta($found_post, 'wpsl_phone', $website['result']['locations'][$i]['phone']);  
                              
                              }else{

                              update_post_meta($found_post, 'wpsl_phone', $website['result']['locations'][$i]['forwardingPhone']);  
                              }
                                                              
                               update_post_meta($found_post, 'wpsl_lat', $website['result']['locations'][$i]['lat']); 
                               update_post_meta($found_post, 'wpsl_lng', $website['result']['locations'][$i]['lng']); 
                               update_post_meta($found_post, 'wpsl_store_shortname', "Big Bob's Flooring - ".$state); 
                               update_post_meta($found_post, 'wpsl_store_shortname', $location_name); 
                               update_post_meta($found_post, 'wpsl_site_url', $location_url);
                               update_post_meta($found_post, 'wpsl_hours', $store_hours_json);
                              

                    }

            }
        
        }

    }    

}

//make_my_store FUnctionality

//add_action( 'wp_ajax_nopriv_make_my_store', 'make_my_store' );
//add_action( 'wp_ajax_make_my_store', 'make_my_store' );

function make_my_store() {
    
     $data = array(); 
     $store_title = get_post_meta( $_POST['store_id'], 'wpsl_store_shortname',true );      
     $store_address = get_post_meta( $_POST['store_id'], 'wpsl_address',true ).' '.get_post_meta($_POST['store_id'],'wpsl_city',true).', '.get_post_meta($_POST['store_id'],'wpsl_state',true).' '.get_post_meta($_POST['store_id'],'wpsl_zip',true);
     $data['store_title']= $store_title;
     $data['header_phone'] = formatPhoneNumber(get_post_meta( $_POST['store_id'], 'wpsl_phone',true ));
     $data['store_address']= $store_address;     
     
     $store_hours_meta = get_post_meta( $_POST['store_id'], 'wpsl_hours',true );      
     $store_hours = maybe_unserialize( $store_hours_meta );
     $currentday = date("l");
   //  write_log($store_title);
    // write_log($store_hours);
    // write_log($currentday);
     
   

     foreach ($store_hours as $mdaKey => $mdaData) {

         if(strtolower($currentday) == $mdaKey){
          

               // write_log($store_title.'----'.$mdaKey . ": " . $mdaData[0]);
                $store_hour = explode(",",$mdaData[0]);
               

                if($store_hour[0] =='Closed' || $store_hour[0] == 'CLOSED'){

                    $data['store_status'] = 'CLOSED';
                    $data['store_hour'] = '';

                }else if($store_hour[0] =='Appointment Only' || $store_hour[0] == 'Appointment'){

                    $data['store_status'] = 'Appointment Only';
                    $data['store_hour'] = '';
                
               }else{

                    $data['store_status'] = 'Open';
                    $data['store_hour'] = $store_hour[0].' - '.$store_hour[1];
                } 

         }      
    }

     echo json_encode($data);    	
    
     wp_die();
}

// Product landing page product list

function product_category_products_function($atts){

    $args = array(
        "post_type" => $atts[0],
        "post_status" => "publish",
        "orderby" => "title",
        "order" => "rand",
        "posts_per_page" => 4
  
      );

      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper landing_page_wrapper">				
				<div class="row product-row">';				
								
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'">';
                          
                          if( get_field('quick_ship') == 1){ 

                            $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';

                                } 

                       $content .='</a>'.brandroomvo_script_integration(get_field('manufacturer') ,get_field('sku') ,get_the_ID()).'</div>';

    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"><span class="color_text"> 
                            '.get_field('color').' </span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>            
                </div>
            </div>';
           
    } 
       
 wp_reset_postdata(); 
						
   $content .= '</div>
			</div>';
            
            return $content;

} 

// Diff collection products

add_shortcode( 'product_listing', 'product_category_products_function' );

function search_distinct() {    global $wpdb;

    return $wpdb->postmeta . '.meta_value ';  }


    
// Vendor page product list

function brand_products_function($atts){

    global $wpdb;

    //write_log( $atts);
    $args = array(
        "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
        "post_status" => "publish",       
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value', 
        "posts_per_page" => 4,
      'meta_query' => array(
                array(
                  'key' => 'brand',
                  'value' => $atts[0],
                  'compare' => '='
                  )
         )
      );

    //  write_log( $args);
    add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';				
                                
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'">';
                          
                            if( get_field('quick_ship') == 1){ 
  
                              $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';
  
                                  } 
  
                         $content .='</a>'.brandroomvo_script_integration(get_field('manufacturer') ,get_field('sku') ,get_the_ID()).'</div>';

                    $table_posts = $wpdb->prefix.'posts';
                    $table_meta = $wpdb->prefix.'postmeta';
                    $familycolor = get_field('collection');  
                    $key = 'collection';  

                    $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

                           //     write_log($coll_sql);

                    $data_collection = $wpdb->get_results($coll_sql);  

    
    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"> <span class="color_text">'.get_field('color').'</span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>'.count($data_collection).' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
           
    } 
       
    wp_reset_postdata(); 
    
    remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';
            
            return $content;
    
    } 
    
    add_shortcode( 'brand_listing', 'brand_products_function' );



    
//Accessibility employee shortcode
function accessibility_shortcodes($arg) {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->contacts as $contact){

        if($contact->accessibility == '1'){ 

            if (in_array("phone", $arg)) {

                $content = '<a href="tel:'.$contact->phone.'">'.$contact->phone.'</a>';                
               
            }
            if (in_array("email", $arg)) {

                $content = '<a href="mailto:'.$contact->email.'">'.$contact->email.'</a>';              
               
            }
        }
    }

    return  $content;
    
}
add_shortcode('accessibility', 'accessibility_shortcodes');

///Make my location functionality of header

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

//add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
//add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );

function get_storelisting() {
  
    global $wpdb;
    $content="";
  
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );
        
        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];
  
        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
  
       //print_r($rawdata);
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'  
                WHERE posts.post_type = 'wpsl_stores' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";
  
              // write_log($sql);
  
        $storeposts = $wpdb->get_results($sql);
        $storeposts_array = json_decode(json_encode($storeposts), true);     
        
        
  
        if(isset($_COOKIE['preferred_store'])){
  
            $store_location = $_COOKIE['preferred_store'];
           

        }else{ 
            $store_location = $storeposts_array['0']['ID'];          
            
           
          }
      
          
          $store_title = str_replace("Flooring -","",get_post_meta( $store_location, 'wpsl_store_shortname', true ));

        $content = '<div class="bb_location_name">'.$store_title.'</div>
        <div class="bb_loc_address">'.get_post_meta( $store_location, 'wpsl_address', true ).' '.get_post_meta( $store_location, 'wpsl_city', true ).', '.get_post_meta( $store_location, 'wpsl_state', true ).' '.get_post_meta( $store_location, 'wpsl_zip', true ).'</div>';     
        

        $content_list = "";

        $i = 1;

    foreach ( $storeposts as $post ) { 

        

        if($_COOKIE['preferred_store'] == $post->ID){

            $class = "activebb";

        }else{

            $class = "";
        }
        
                 
                  $content_list .= '<div class="location-section '.$class.'" id ="'.$post->ID.'" data-count="'.$i.'">
                  <div class="makemystore_header">                  
                  <input type="radio" data-store-id"="'.$post->ID.'" id="'.$post->ID.'_mystore" class="mystore_radio makemystore_radio" name="mystore" value="'.$post->ID.'">
                  <label for="'.$post->ID.'_mystore"></label>
                  </div>
                  <div class="location-info-section">
                      <a href="'.get_permalink($post->ID).'" class="location-name">'.str_replace("Flooring -","",get_post_meta( $post->ID, 'wpsl_store_shortname', true )).' - '.round($post->distance,1).' MI</a>
                      <p class="store-add"> '.get_post_meta( $post->ID, 'wpsl_address', true ).' '.get_post_meta( $post->ID, 'wpsl_city', true ).', '.get_post_meta( $post->ID, 'wpsl_state', true ).' '.get_post_meta( $post->ID, 'wpsl_zip', true ).'</p>';

                      if(get_post_meta( $post->ID, 'wpsl_phone', true ) != ''){
                      $content_list .= '<p class="store-phone"><a class="phone_bb" href="tel:'.get_post_meta( $post->ID, 'wpsl_phone', true ).'">'.get_post_meta( $post->ID, 'wpsl_phone', true ).'</a></p>';
                      }

                      $content_list .=  '</div>
              </div>'; 

              $i++;
    } 
    
  
    $data = array();

    $store_hours_meta = get_post_meta( $store_location, 'wpsl_hours',true );      
     $store_hours = maybe_unserialize( $store_hours_meta );
     $currentday = date("l");
   //  write_log($store_title);
    // write_log($store_hours);
    // write_log($currentday);
     

    foreach ($store_hours as $mdaKey => $mdaData) {

        if(strtolower($currentday) == $mdaKey){
         

              // write_log($store_title.'----'.$mdaKey . ": " . $mdaData[0]);
               $store_hour = explode(",",$mdaData[0]);
              

               if($store_hour[0] =='Closed' || $store_hour[0] == 'CLOSED'){

                   $data['store_status'] = 'CLOSED';
                   $data['store_hour'] = '';

               }else if($store_hour[0] =='Appointment Only' || $store_hour[0] == 'Appointment'){

                   $data['store_status'] = 'Appointment Only';
                   $data['store_hour'] = '';
               
              }else{

                   $data['store_status'] = 'Open';
                   $data['store_hour'] = $store_hour[0].' - '.$store_hour[1];
               } 

        }      
   }
    $data['store_title']= get_the_title( $store_location);   
    $data['header'] = $content;
    $data['list'] = $content_list;
    $data['store_location'] = get_post_meta( $store_location, 'wpsl_store_shortname', true );
    $data['header_phone'] = formatPhoneNumber(get_post_meta( $store_location, 'wpsl_phone', true ));
    $data['distance'] = round($post->distance,1);
    $data['store_name'] = get_post_meta( $store_location, 'wpsl_store_shortname', true );
    $data['address'] =  get_post_meta( $store_location, 'wpsl_address', true ).''.get_post_meta( $store_location, 'wpsl_city', true ).', '.get_post_meta( $store_location, 'wpsl_state', true ).' '.get_post_meta( $store_location, 'wpsl_zip', true );
    $data['store_id']=  $store_location;

   // write_log($data['list']);
  
    echo json_encode($data);
        wp_die();
  }


  //Quick ship sync

add_action( 'init', 'register_bb_quickship_cron_delete_event');

// Function which will register the event
function register_bb_quickship_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_quickship_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_quickship_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_quickship_hook_activation');

/**  Function for activate cron job  **/
function bb_quickship_hook_activation() {
    if (! wp_next_scheduled ( 'bb_quickship_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_quickship_sync');
    }
}


add_action( 'bb_quickship_sync', 'bb_quickship_sync_function' );

function bb_quickship_sync_function(){

    global $wpdb;
    $upload_dir = wp_upload_dir(); 
    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_group = $wpdb->prefix.'redirection_groups';		
    $table_posts = $wpdb->prefix.'posts';
    $table_meta = $wpdb->prefix.'postmeta';
    
    $permfile = $upload_dir.'/quickship.csv';

    $FileContent = @fopen($upload_dir['basedir'].'/'.'sfn-data/quickship.csv', "r");

   // write_log($FileContent);

    $i=0;

    if ($FileContent) {

            while (($row = fgetcsv($FileContent, 1000)) !== false) {

                if($row[1] !='' && $row[0] == 1){

                    $find_sku = $row[1];

                    $sql_sku = "SELECT $table_meta.post_id 
                    FROM $table_meta
                    WHERE  $table_meta.meta_key = 'sku' 
                    AND $table_meta.meta_value = '$find_sku'" ;	
                    
                    $sku_result = $wpdb->get_results($sql_sku,ARRAY_A);	
                
                        if (count($sku_result)> 0){
                
                            write_log('Product in Catalog-'.$find_sku);

                            $post_id = $sku_result[0]['post_id'];
                            
                            update_post_meta($post_id, 'quick_ship', '1'); 
                
                        }else{

                            write_log('Product not in Catalog-'.$find_sku);

                        }

                }

                    $i++;
            }

        }
}

//add_filter( 'gform_pre_render_4', 'populate_product_location_form' );
//add_filter( 'gform_pre_validation_4', 'populate_product_location_form' );
//add_filter( 'gform_pre_submission_filter_4', 'populate_product_location_form' );
//add_filter( 'gform_admin_pre_render_4', 'populate_product_location_form' );
function populate_product_location_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if( $field['id'] == 13 ) {           	

            $args = array(
                'post_type'      => 'wpsl_stores',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

             // write_log($choices);

             // Set placeholder text for dropdown
             $field->placeholder = '-- Choose Location --';

             // Set choices from array of ACF values
             $field->choices = $choices;

        }
   }
return $form;

}



// Area Rug Gallery Images
function bb_financing_page_function(){

    $website_json =  json_decode(get_option('website_json'));


    if(isset($website_json->financeWF) &&  $website_json->financeWF != ''){

        echo do_shortcode('[fl_builder_insert_layout slug="wellsfargo_financing"]');

    }elseif(isset($website_json->financeSync) &&  $website_json->financeSync != ''){

        echo do_shortcode('[fl_builder_insert_layout slug="synchrony_financing"]');

    }elseif(isset($website_json->financeGeneric) && ($website_json->financeGeneric == 'true' || $website_json->financeGeneric == '1')){

        echo do_shortcode('[fl_builder_insert_layout slug="generic_financing"]');

    }
    
}
add_shortcode('bb_financing', 'bb_financing_page_function');

//Roomvo script integration
function brandroomvo_script_integration($manufacturer,$sku,$pro_id){

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){

                    if($manufacturer == 'Bruce'){ 
                      $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer == 'AHF'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer  == 'Shaw'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    else{
                        $manufacturer == $manufacturer ;
                    }

                    $manufacturer = str_replace(' ', '', strtolower($manufacturer));  

                    $content='<div id="roomvo">
                            <div class="roomvo-container">
                        <a class="roomvo-stimr button" data-sku="'.$manufacturer.'-'.$sku.'" style="visibility: hidden;"><i class="fa fa-camera" aria-hidden="true"></i><span class="room_span"> &nbsp;VIEW IN ROOM</span></a>
                            </div>
                    </div>';
       
               $content .='<script type="text/javascript">
                               function getProductSKU() {                       
                               return "'.$manufacturer.'-'.$sku.'";
                               }
                           </script>';
        
                 return $content;
    
            }
    
        }
    }
   
}

  // Vendor menu list
function bb_brand_image_items() {
    ob_start();
    // $vendor_page = get_page_by_path( '/vendor/', OBJECT, 'page' );
    ?> 
   <ul class="vendor_brand_image_list">
        <li class="brand_section_hide dixie-home"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/dixie_logo.png" alt="dixie_logo" title="dixie_logo"></li>
        <li class="brand_section_hide phenix"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/phenix_logo.png" alt="phenix_logo" title="phenix_logo"></li>
        <li class="brand_section_hide daltile"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/daltile_logo.png" alt="daltile_logo" title="daltile_logo"></li>
        <li class="brand_section_hide engineered-floors"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/engineered_logo.png" alt="engineered_logo" title="engineered_logo"></li>
        <li class="brand_section_hide mannington"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/mannington_logo.png" alt="mannington_logo" title="mannington_logo"></li>
        <li class="brand_section_hide mohawk"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/mohawk_logo.png" alt="mohawk_logo" title="mohawk_logo"></li>
        <li class="brand_section_hide american-olean"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/americanOlean_logo.png" alt="american-olean_logo" title="american-olean_logo"></li>
        <li class="brand_section_hide flooring-one-source"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/Flooring-One-Source-Logo.jpg" alt="flooring-one-source" title="flooring-one-source"></li>
        <li class="brand_section_hide happy-feet"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/happyfeet.png" alt="happy-feet_logo" title="happy-feet_logo"></li>
        <li class="brand_section_hide lifescape-designs"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/LifeScapeDesigns-Logo.jpg" alt="lifescape-designs" title="lifescape-designs"></li>
        <li class="brand_section_hide eternally"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/Eternally-Logo.jpg" alt="eternally" title="eternally"></li>
        <li class="brand_section_hide landmark"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/LandmarkCollection-Logo.jpg" alt="LandmarkCollection" title="LandmarkCollection"></li>
        <li class="brand_section_hide healthier-choice"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/healthier-choice-logo.jpg" alt="healthier-choice" title="healthier-choice"></li>
        <li class="brand_section_hide leggett-platt"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/Leggett-Platt-logo.jpg" alt="leggett-platt" title="leggett-platt"></li>
        <li class="brand_section_hide marazzi"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/marazzi_logo.jpg" alt="marazzi_logo" title="marazzi_logo"></li>
        <li class="brand_section_hide nance"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/nance-logo.jpg" alt="nance_logo" title="nance_logo"></li>
        <li class="brand_section_hide nourison"><img src="https://mmllc-images.s3.amazonaws.com/promos/tstprmtn1/select_3348_nourisonlogo.png" alt="nourison" title="nourison"></li>
        <li class="brand_section_hide peerless peerless-flooring"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/peerless-flooring-logo.jpg" alt="peerless-flooring" title="peerless-flooring"></li>
        <li class="brand_section_hide shaw shaw-floors shaw-flooring"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/shaw_logo.png" alt="shaw-flooring" title="shaw-flooring"></li>
    </ul>
    <?php
    return ob_get_clean();
}
add_shortcode( 'BRAND_IMAGE_ITEMS', 'bb_brand_image_items' );

/**
* Hide Draft Pages from the menu
*/
function filter_draft_pages_from_menu ($items, $args) {
    foreach ($items as $ix => $obj) {
     if ( 'draft' == get_post_status ($obj->object_id)) {
      unset ($items[$ix]);
     }
    }
    return $items;
   }
   add_filter ('wp_nav_menu_objects', 'filter_draft_pages_from_menu', 10, 2);


// Kitchen windows pages and child pages cron job
add_action( 'init', 'register_f2c_cron_delete_event');

// Function which will register the event
function register_f2c_cron_delete_event() {
	if( !wp_next_scheduled( 'f2c_kitchen_window_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'hourly', 'f2c_kitchen_window_sync' );
    }
}
   

register_activation_hook(__FILE__, 'f2c_hook_activation');

/**  Function for activate cron job  **/
function f2c_hook_activation() {
    if (! wp_next_scheduled ( 'f2c_kitchen_window_sync' )) {
	    wp_schedule_event(time(), 'hourly', 'f2c_kitchen_window_sync');
    }
}


add_action( 'f2c_kitchen_window_sync', 'f2c_kitchen_window_sync_function' );


function f2c_kitchen_window_sync_function() {
    
    $website_json =  json_decode(get_option('website_json'));

    $website_options = $website_json->options->kitchenBath;

    if($website_json->options->windowTreatment == '1'){

        $windowTreatment_page = get_page_by_path( '/window-treatments/', OBJECT, 'page' );
        $windowTreatment = array( 'ID' => $windowTreatment_page->ID, 'post_status' => 'publish' );
        wp_update_post($windowTreatment);

        $shadespage = get_page_by_path( '/window-treatments/shades/', OBJECT, 'page' );
        $shades = array( 'ID' => $shadespage->ID, 'post_status' => 'publish' );
        wp_update_post($shades);

        $blindspage = get_page_by_path( '/window-treatments/blinds/', OBJECT, 'page' );
        $blinds = array( 'ID' => $blindspage->ID, 'post_status' => 'publish' );
        wp_update_post($blinds);

        $shutterspage = get_page_by_path( '/window-treatments/shutters/', OBJECT, 'page' );
        $shutters = array( 'ID' => $shutterspage->ID, 'post_status' => 'publish' );
        wp_update_post($shutters);

        $draperypage = get_page_by_path( '/window-treatments/drapery/', OBJECT, 'page' );
        $drapery = array( 'ID' => $draperypage->ID, 'post_status' => 'publish' );
        wp_update_post($drapery);

    }else{

        $windowTreatment_page = get_page_by_path( '/window-treatments/', OBJECT, 'page' );
        $windowTreatment = array( 'ID' => $windowTreatment_page->ID, 'post_status' => 'draft' );
        wp_update_post($windowTreatment);

        $shadespage = get_page_by_path( '/window-treatments/shades/', OBJECT, 'page' );
        $shades = array( 'ID' => $shadespage->ID, 'post_status' => 'draft' );
        wp_update_post($shades);

        $blindspage = get_page_by_path( '/window-treatments/blinds/', OBJECT, 'page' );
        $blinds = array( 'ID' => $blindspage->ID, 'post_status' => 'draft' );
        wp_update_post($blinds);

        $shutterspage = get_page_by_path( '/window-treatments/shutters/', OBJECT, 'page' );
        $shutters = array( 'ID' => $shutterspage->ID, 'post_status' => 'draft' );
        wp_update_post($shutters);

        $draperypage = get_page_by_path( '/window-treatments/drapery/', OBJECT, 'page' );
        $drapery = array( 'ID' => $draperypage->ID, 'post_status' => 'draft' );
        wp_update_post($drapery);

    }

    if($website_json->options->kitchenBath == '1'){

        $kitchenBath_page = get_page_by_path( '/kitchen-and-bath/', OBJECT, 'page' );
        $kitchenBath = array( 'ID' => $kitchenBath_page->ID, 'post_status' => 'publish' );
        wp_update_post($kitchenBath);

        if( $website_json->options->kitchenBathCountertop == '1' ){

            $kitchenBathCountertop_page = get_page_by_path( '/countertops/', OBJECT, 'page' );
            $kitchenBathCountertop = array( 'ID' => $kitchenBathCountertop_page->ID, 'post_status' => 'publish' );
            wp_update_post($kitchenBathCountertop);
    
    
        }else{

            $kitchenBathCountertop_page = get_page_by_path( '/countertops/', OBJECT, 'page' );
            $kitchenBathCountertop = array( 'ID' => $kitchenBathCountertop_page->ID, 'post_status' => 'draft' );
            wp_update_post($kitchenBathCountertop);
        }

        if( $website_json->options->kitchenBathCabinet == '1' ){
    
            $kitchenBathCabinet_page = get_page_by_path( '/cabinets/', OBJECT, 'page' );
            $kitchenBathCabinet = array( 'ID' => $kitchenBathCabinet_page->ID, 'post_status' => 'publish' );
            wp_update_post($kitchenBathCabinet);
    
            
        }else{

            $kitchenBathCabinet_page = get_page_by_path( '/cabinets/', OBJECT, 'page' );
            $kitchenBathCabinet = array( 'ID' => $kitchenBathCabinet_page->ID, 'post_status' => 'draft' );
            wp_update_post($kitchenBathCabinet);

        }

        if( $website_json->options->kitchenBathVanity == '1' ){
    
            $kitchenBathVanity_page = get_page_by_path( '/vanities/', OBJECT, 'page' );
            $kitchenBathVanity = array( 'ID' => $kitchenBathVanity_page->ID, 'post_status' => 'publish' );
            wp_update_post($kitchenBathVanity);
    
            
        }else{

            $kitchenBathVanity_page = get_page_by_path( '/vanities/', OBJECT, 'page' );
            $kitchenBathVanity = array( 'ID' => $kitchenBathVanity_page->ID, 'post_status' => 'draft' );
            wp_update_post($kitchenBathVanity);

        }
        
        if( $website_json->options->kitchenBathHardware == '1' ){
    
            $hardware_page = get_page_by_path( '/hardware/', OBJECT, 'page' );
            $hardware = array( 'ID' => $hardware_page->ID, 'post_status' => 'publish' );
            wp_update_post($hardware);    
            
        }else{

            $hardware_page = get_page_by_path( '/hardware/', OBJECT, 'page' );
            $hardware = array( 'ID' => $hardware_page->ID, 'post_status' => 'draft' );
            wp_update_post($hardware);    

        }

    }else{

        $kitchenBath_page = get_page_by_path( '/kitchen-and-bath/', OBJECT, 'page' );
        $kitchenBath = array( 'ID' => $kitchenBath_page->ID, 'post_status' => 'draft' );
        wp_update_post($kitchenBath);

        $kitchenBathCountertop_page = get_page_by_path( '/countertops/', OBJECT, 'page' );
        $kitchenBathCountertop = array( 'ID' => $kitchenBathCountertop_page->ID, 'post_status' => 'draft' );
        wp_update_post($kitchenBathCountertop);

        $kitchenBathCabinet_page = get_page_by_path( '/cabinets/', OBJECT, 'page' );
        $kitchenBathCabinet = array( 'ID' => $kitchenBathCabinet_page->ID, 'post_status' => 'draft' );
        wp_update_post($kitchenBathCabinet);

        $kitchenBathVanity_page = get_page_by_path( '/vanities/', OBJECT, 'page' );
        $kitchenBathVanity = array( 'ID' => $kitchenBathVanity_page->ID, 'post_status' => 'draft' );
        wp_update_post($kitchenBathVanity);

        $hardware_page = get_page_by_path( '/hardware/', OBJECT, 'page' );
        $hardware = array( 'ID' => $hardware_page->ID, 'post_status' => 'draft' );
        wp_update_post($hardware); 

    }  

   
}

//today store hours in header

add_action( 'wp_ajax_nopriv_store_today_hour', 'store_today_hours' );
add_action( 'wp_ajax_store_today_hour', 'store_today_hours' );

function store_today_hours() {
    
    $website = json_decode(get_option('website_json'));   
    $data = array();    
    $currentday = date("l");
    
    for ($i=0;$i<count($website->locations);$i++) {       

        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){

            for ($j = 0; $j < count($website->locations[$i]->hours); $j++) {

             //   write_log(strtolower($currentday));

               // write_log($website->locations[$i]->hours[$j]->day.'-'.$website->locations[$i]->hours[$j]->hours);

                if(strtolower($currentday) == strtolower($website->locations[$i]->hours[$j]->day)){                    

                    $data['store_hour'] = $website->locations[$i]->hours[$j]->hours;
                    $data['store_day'] = $website->locations[$i]->hours[$j]->day;

                }

            }
        }
    }   
    
    echo json_encode($data);    	
   
    wp_die();
}

function f2c_finnacing_card_function() {
   
    $website_json =  json_decode(get_option('website_json'));


    if(isset($website_json->financeWF) &&  $website_json->financeWF != ''){
       
       return  'https://mmllc-images.s3.amazonaws.com/promos/bgbbsdmptstngnly/select_3255_wellsfinancing2x1.png';      

    }elseif(isset($website_json->financeSync) && $website_json->financeSync != ''){

        return  'https://mmllc-images.s3.amazonaws.com/promos/bgbbsdmptstngnly/select_3254_synccard2xmin.png';
      

    }elseif(isset($website_json->financeGeneric) && $website_json->financeGeneric == 1){

        return  'https://mmllc-images.s3.amazonaws.com/promos/bgbbsdmptstngnly/select_3255_bigbobsfinancing2x.png';       

    }

}
add_shortcode( 'financing_card', 'f2c_finnacing_card_function' );

// Product Landing pages and child pages cron job
add_action( 'init', 'register_f2c_landing_page_cron_delete_event');

// Function which will register the event
function register_f2c_landing_page_cron_delete_event() {
	if( !wp_next_scheduled( 'f2c_product_landing_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'hourly', 'f2c_product_landing_sync' );
    }
}
   

register_activation_hook(__FILE__, 'f2c_product_landing_hook_activation');

/**  Function for activate cron job  **/
function f2c_product_landing_hook_activation() {
    if (! wp_next_scheduled ( 'f2c_product_landing_sync' )) {
	    wp_schedule_event(time(), 'hourly', 'f2c_product_landing_sync');
    }
}


add_action( 'f2c_product_landing_sync', 'f2c_product_landing_sync_function' );


function f2c_product_landing_sync_function() {
    
    $website_json =  json_decode(get_option('website_json'));

    $product_landing_options = array(
        'carpetLanding' => array(
            'landing' => '/flooring/carpet/',
            'plp' => '/flooring/carpet/products/'
        ),
        'hardwoodLanding' => array(
            'landing' => '/flooring/hardwood/',
            'plp' => '/flooring/hardwood/products/'
        ),
        'laminateLanding' => array(
            'landing' => '/flooring/laminate/',
            'plp' => '/flooring/laminate/products/'
        ),
        'vinylLanding' => array(
            'landing' => '/flooring/vinyl/',
            'plp' => '/flooring/vinyl/products/'
        ),
        'tileLanding' => array(
            'landing' => '/flooring/tile/',
            'plp' => '/flooring/tile/products/'
        ),
        'areaRugsLanding' => array(
            'landing' => '/flooring/area-rugs/'
        )
    );

    $productLandings = json_decode(json_encode($website_json->options),true);
    // write_log($productLandings);
    $page_status = array('draft','publish');
    if(is_array($productLandings) && count($productLandings) > 0){
        foreach($productLandings as $cat => $status){
            if(is_array(@$product_landing_options[$cat]) && count(@$product_landing_options[$cat]) > 0){
                foreach($product_landing_options[$cat] as $key => $value){
                    $url_path = $value;
                    $found_page = get_page_by_path( $url_path, OBJECT, 'page' );
                    $args = array( 'ID' => $found_page->ID, 'post_status' => $page_status[$status] );
                    wp_update_post($args);
                }
            }
        } 
    }else{
        foreach($product_landing_options as $key => $landingpages){
            foreach($landingpages as $url){
                $found_page = get_page_by_path( $url, OBJECT, 'page' );
                if(isset($found_page->ID)){
                    $args = array( 'ID' => $found_page->ID, 'post_status' => $page_status[0] );
                    wp_update_post($args);
                }
            }
        }
    }
}


add_shortcode('f2c_banner', 'floortoceiling_banner_function');

function floortoceiling_banner_function($arg){

    $salebannerdata = json_decode(get_option('salebannerdata'));    
    $div='';  
  
       foreach($salebannerdata as $bannerinfo){       
      
            if(in_array('kitchen',$arg) && $bannerinfo->category == 'kitchenBath'){     
            

                        if($bannerinfo->images->desktop !='' && $bannerinfo->images->mobile !=''){                           
                            
                            $div = "<div class='salebanner banner-deskop fixed'><a href='".$bannerinfo->link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$bannerinfo->images->desktop." 4025w, 
                            https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$bannerinfo->images->desktop." 3019w, 
                            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop." 2013w, 
                            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop." 1006w' 
                            
                            src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop."'  style='width:100%;text-align:center' alt='sales' />
                            </a></div>";
        
                    
                            $div .= "<div class='salebanner banner-mobile'><a href='".$bannerinfo->link."'><img src='".$bannerinfo->images->mobile."' alt='sales' /></a></div>";

                      }
            }else if(in_array('window',$arg) && $bannerinfo->category == 'windowTreatment'){     
            

                if($bannerinfo->images->desktop !='' && $bannerinfo->images->mobile !=''){                           
                    
                    $div = "<div class='salebanner banner-deskop fixed'><a href='".$bannerinfo->link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$bannerinfo->images->desktop." 4025w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$bannerinfo->images->desktop." 3019w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop." 2013w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop." 1006w' 
                    
                    src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop."'  style='width:100%;text-align:center' alt='sales' />
                    </a></div>";

            
                    $div .= "<div class='salebanner banner-mobile'><a href='".$bannerinfo->link."'><img src='".$bannerinfo->images->mobile."' alt='sales' /></a></div>";

              }
         }else if(in_array('flooring',$arg) && $bannerinfo->category == 'flooring'){     
            

            if($bannerinfo->images->desktop !='' && $bannerinfo->images->mobile !=''){                           
                
                $div = "<div class='salebanner banner-deskop fixed'><a href='".$bannerinfo->link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$bannerinfo->images->desktop." 4025w, 
                https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$bannerinfo->images->desktop." 3019w, 
                https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop." 2013w, 
                https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop." 1006w' 
                
                src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$bannerinfo->images->desktop."'  style='width:100%;text-align:center' alt='sales' />
                </a></div>";

        
                $div .= "<div class='salebanner banner-mobile'><a href='".$bannerinfo->link."'><img src='".$bannerinfo->images->mobile."' alt='sales' /></a></div>";

          }
     }

        }

        return $div;

    }
          
   // if /promotion/?promo=expiredsalecode then it will redirect     

    function expired_promo_redirect(){

        $promos = json_decode(get_option('promos_json'));

        $active_promo = array();
       
        if (isset($_GET['promo']) && $_GET['promo']!='' && $promos !='') {

            foreach($promos as $promo){

                $active_promo[] = $promo->promoCode;
                
            }

            if(!in_array($_GET['promo'], $active_promo)){                
              
                write_log(' not active promo');
                 wp_redirect(home_url());
             }
        } // end check
    } // end of function
    
    
    // submit record on init hook
    add_action('init', 'expired_promo_redirect');

    function bb_brand_image_carousel() {
        ob_start();

        $product_json =  json_decode(get_option('product_json'));

       // write_log($product_json);
    
        
        ?> 
         <div class="brand_image_slider">
         <div class="slides">

         <?php 
         
         $brand_list = array();

         foreach($product_json as $brand){

            //foreach ($brand as $key => $value) {

         //   write_log($brand);
           // write_log($brand['manufacturer']);
          //  write_log($brand->manufacturer);

           $pro_brand_string = str_replace(" ", "-", $brand->manufacturer);
           $pro_brand = strtolower($pro_brand_string);

           $brand_list[] = $pro_brand ;

         //   }

         }

        //  write_log($brand_list);
         ?>

        
           <?php if (in_array("dixie-home", $brand_list)){ ?>
            <div class="slide  dixie-home"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/dixie_logo.png" alt="dixie_logo" title="dixie_logo"></div>
           <?php } ?>
           <?php if (in_array("phenix", $brand_list)){ ?>
            <div class="slide  phenix"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/phenix_logo.png" alt="phenix_logo" title="phenix_logo"></div>
            <?php } ?>
           <?php if (in_array("daltile", $brand_list)){ ?>
            <div class="slide  daltile"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/daltile_logo.png" alt="daltile_logo" title="daltile_logo"></div>
            <?php } ?>
           <?php if (in_array("engineered-floors", $brand_list)){ ?>
            <div class="slide  engineered-floors"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/engineered_logo.png" alt="engineered_logo" title="engineered_logo"></div>
            <?php } ?>
           <?php if (in_array("mannington", $brand_list)){ ?>
            <div class="slide  mannington"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/mannington_logo.png" alt="mannington_logo" title="mannington_logo"></div>
            <?php } ?>
           <?php if (in_array("mohawk", $brand_list)){ ?>
            <div class="slide brand_section_hide mohawk"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/mohawk_logo.png" alt="mohawk_logo" title="mohawk_logo"></div>
            <?php } ?>
           <?php if (in_array("american-olean", $brand_list)){ ?>
            <div class="slide  american-olean"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/americanOlean_logo.png" alt="american-olean_logo" title="american-olean_logo"></div>
            <?php } ?>
           <?php if (in_array("flooring-on-source", $brand_list)){ ?>
            <div class="slide  flooring-on-source"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="flooring-on-source" title="flooring-on-source"></div>
            <?php } ?>
           <?php if (in_array("happy-feet", $brand_list)){ ?>
            <div class="slide  happy-feet"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/happyfeet.png" alt="happy-feet_logo" title="happy-feet_logo"></div>
            <?php } ?>
           <?php if (in_array("healthier-choice", $brand_list)){ ?>
            <div class="slide  healthier-choice"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="healthier-choice" title="healthier-choice"></div>
            <?php } ?>
           <?php if (in_array("laggett-platt", $brand_list)){ ?>
            <div class="slide  laggett-platt"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="laggett-platt" title="laggett-platt"></div>
            <?php } ?>
           <?php if (in_array("marazzi", $brand_list)){ ?>
            <div class="slide marazzi"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/marazzi_logo.jpg" alt="marazzi_logo" title="marazzi_logo"></div>
            <?php } ?>
           <?php if (in_array("nance", $brand_list)){ ?>
            <div class="slide  nance"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="nance_logo" title="nance_logo"></div>
            <?php } ?>
           <?php if (in_array("nourison", $brand_list)){ ?>
            <div class="slide  nourison"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/nourison_logo.jpg" alt="nourison" title="nourison"></div>
            <?php } ?>
           <?php if (in_array("shaw-flooring", $brand_list)){ ?>
            <div class="slide  shaw-flooring"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/shaw_logo.png" alt="shaw-flooring" title="shaw-flooring"></div>
            <?php } ?>
           
        
    </div>
        <?php
        return ob_get_clean();
    }
    add_shortcode( 'BRAND_IMAGE_CAROUSEL', 'bb_brand_image_carousel' );




    function current_promos_function($arg){
        ob_start();
        $seleinformation = json_decode(get_option('salesliderinformation'));
    
        usort($seleinformation, 'compare_cde_some_objects');		
    
        $website_json =  json_decode(get_option('website_json'));
        $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
        $loc_country = $website_json->locations[0]->country;
        if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }
      ?>
    
    <ul class="current_promos">
    <?php  
        if( count($seleinformation) > 0 ){	  	
            foreach($seleinformation as $slide){
                if($slide->isRugShop == 'true'){
                    $target = "_blank";
                    if($rugAffiliateCode!=''){
                        if($slide->slide_link != ''){
                            $slide->slide_link = 'https://rugs.shop/'.$en.'/'.trim($slide->slide_link , '/').'/?store='.$rugAffiliateCode;
                        }else{
    
                            $slide->slide_link = 'https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode;
                        }
                    }else{
                        $slide->slide_link = 'https://rugs.shop/';
                    }
                }else{
                    $target = "_self";
                    $slide->slide_link = $slide->slide_link;
                }
    
                $slide_coupon_img = "https://mm-media-res.cloudinary.com/image/fetch/h_440,w_440,c_limit/".$slide->slider->slider_coupan_img;
                
                ?> 
                <li class="current_promo_item"> 
                    <!-- <div class="current_promo_image" style="background-image: url(<?php echo $slide_coupon_img;?>);"> -->
                        <!-- <div class="current-promo-wrap">
                            <h3 class="current_promo_title"><?php echo $slide->name;?></h3>
                            <a class="current_promo_link fl-button" href="<?php echo $slide->slide_link;?>">Learn More</a>
                        </div> -->
                        
                    <!-- </div> -->
                    <a href="<?php echo $slide->slide_link;?>">
                            <img class="current_promo_image" src="<?php echo $slide_coupon_img;?>" alt="<?php echo $slide->name;?>">
                    </a>
                </li> 
                <?php
            }
            ?>
            
            <?php
        } ?>
                <li class="current_promos_item">
    
                
                <div class="current_promo_image" style="background-image: url(https://mmllc-images.s3.amazonaws.com/promos/tstprmtn1/mobile_3348_finacingblock.jpg);">
                        <div class="current-promo-wrap">
                        <h3 class="current_promo_title">Financing</h3>
                        <a class="current_promo_link fl-button" href="/flooring-financing/">Learn More</a>
                        </div>	
                    </div>
                
            </li>
            </ul> 
                 
         <?php return ob_get_clean();

    }
    
add_shortcode('CURRENT_PROMOS', 'current_promos_function');
    

// Swell chat widget code
function swell_chat_function() {
  
    $social_report = json_decode(get_option('social_report'));

        foreach( $social_report as $report){

            if($report->type == 'chat' && $report->active == '1' && $report->platform == 'swell' && $report->script != ''){     

                $script = str_replace('\"', '', $report->script);

                echo $report->script;
               
            }
        }
    }

add_action('wp_footer', 'swell_chat_function'); 


// Swell review widget code

function swell_review_function() {
  
    $social_report = json_decode(get_option('social_report'));

        foreach( $social_report as $report){                      
           
            if($report->type == 'review' && $report->active == '1' && $report->platform == 'swell' && $report->script != ''){                

                return $report->script;
               
            }
        }
    }

add_shortcode('swell_review', 'swell_review_function');


//sale heading shortcodes

function sale_landing_heading($arg){

    $promos = json_decode(get_option('promos_json'));

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);

    if(isset($_GET['promo']) && !empty($_GET['promo'])) {

         if (in_array('heading', $arg)) {

            if(isset($_GET['promo']) && !empty($_GET['promo'])) {

                foreach($promos as $promo){ 

                if($promo->promoCode == $_GET['promo'] ){

                    foreach($promo->widgets as $widget){
                        
                        if($widget->type == "landing" ){
                            
                            foreach($widget->texts as $text){

                                if($text->type == 'heading'){

                                    $content = $text->text;

                                }                           

                            }

                        }

                    }

                }


            }
        }
        
    }

    if (in_array('subheading', $arg)) {

        if(isset($_GET['promo']) && !empty($_GET['promo'])) {

            foreach($promos as $promo){ 

            if($promo->promoCode == $_GET['promo'] ){

                foreach($promo->widgets as $widget){
                    
                    if($widget->type == "landing" ){
                        
                        foreach($widget->texts as $text){

                            if($text->type == 'subheading'){

                                $content = $text->text;

                            }                           

                        }

                    }

                }

            }


        }
    }
    
}
    }else{

        $promo_code = array_search($high_priprity,$salespriority);

        if (in_array('heading', $arg)) {

            foreach($promos as $promo){ 

                if($promo->promoCode == $promo_code ){


                    foreach($promo->widgets as $widget){
                        
                        if($widget->type == "landing" ){
                            
                            foreach($widget->texts as $text){

                                if($text->type == 'heading'){

                                    $content = $text->text;

                                }                           

                            }

                        }

                    }


                }
            }

        }

        if (in_array('subheading', $arg)) {

            foreach($promos as $promo){ 

                if($promo->promoCode == $promo_code ){


                    foreach($promo->widgets as $widget){
                        
                        if($widget->type == "landing" ){
                            
                            foreach($widget->texts as $text){

                                if($text->type == 'subheading'){

                                    $content = $text->text;

                                }                           

                            }

                        }

                    }


                }
            }

        }

    }

    return $content;

}
add_shortcode('sale_heading', 'sale_landing_heading');