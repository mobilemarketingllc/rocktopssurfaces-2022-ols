<?php

class GPLS_Compatibility_GravityImport {

	public function __construct() {
		if ( ! class_exists( 'GravityKit\GravityImport\UI' ) ) {
			return;
		}

		$this->add_filters();
	}

	public function add_filters() {
		add_filter( 'gk/gravityimport/ui/form-data', array( $this, 'process_feed_name' ), 10, 3 );
	}

	/**
	 * @param array $form_data From data with available fields and feeds.
	 *
	 * @return array
	 */
	public function process_feed_name( $form_data ) {
		// Only to be processed for Limit Submissions feeds.
		if ( ! isset( $form_data['form_feeds']['Limit Submissions'] ) ) {
			return $form_data;
		}
		foreach ( $form_data['form_feeds']['Limit Submissions'] as &$feed ) {
			$ls_feed      = GFAPI::get_feed( $feed['id'] );
			$feed['name'] = $ls_feed['meta']['rule_group_name'];
		}
		return $form_data;
	}

}
