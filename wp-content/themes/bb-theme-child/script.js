// header flyer choose location js start here 
jQuery(document).ready(function() { 

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=store_today_hour',
        dataType: 'JSON',

        success: function(data) {          

         var posts = JSON.parse(JSON.stringify(data));        
         
         jQuery(".fc_timing").html( posts.store_day +' '+ posts.store_hour); 

                     	
        }
    });
	
    jQuery('.searchIcon .fl-icon').click(function() {
        jQuery('.searchModule').slideToggle();
    });

    jQuery(document).mouseup(function(e) {
        var container = jQuery(".searchModule, .searchIcon");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0 && jQuery('.searchModule').css('display') !== 'none') {
            jQuery('.searchModule').slideToggle();
        }
    });

    // Request Popup - Custom JS
    jQuery('.product-form-link').each(function(){
        jQuery(this).find('a.pp-coupon-link').attr('href','javascript:void(0)').addClass('product-form-open');
    });

    jQuery('.product-form-open').click(function(){
        jQuery('.request-quote-form a.uabb-button').trigger('click');
        var getProductTitle = jQuery(this).parents('.pp-coupon:eq(0)').find('.pp-coupon-title').html();
        jQuery('.product-interest-field input').val(getProductTitle.replace(/\s+/g, " "));
        jQuery('.product-interest-field input').attr('value', getProductTitle.replace(/\s+/g, " ") );
        jQuery('.product-interest-field input').attr('readonly', true);
    });

    jQuery('.countertop-request-form-open').click(function(){
        console.log('trigger');
        jQuery('.request-quote-form a.uabb-button').trigger('click');
        var getProductTitle = jQuery(this).siblings('h2.countertop_product_title').html();
        jQuery('.product-interest-field input').val(getProductTitle.replace(/\s+/g, " "));
        jQuery('.product-interest-field input').attr('value', getProductTitle.replace(/\s+/g, " ") );
        jQuery('.product-interest-field input').attr('readonly', true);
    });

    jQuery('.uabb-image-carousel .uabb-image-carousel-item').each(function(){
        jQuery(this).children('.uabb-image-carousel-caption').children('.alt-button').wrap('<div class="alt-button-wrap" />');
        var getAltButtonHTML = jQuery(this).children('.uabb-image-carousel-caption').children('.alt-button-wrap').html();
        if(getAltButtonHTML!=undefined || getAltButtonHTML!="undefined"){
            jQuery(this).prepend(getAltButtonHTML);
            jQuery(this).children('.uabb-image-carousel-caption').children('.alt-button-wrap').hide();
            jQuery(this).children('.alt-button').addClass('button');
        }
    });
});

(function($) {
    document.addEventListener('facetwp-loaded', function() {
        jQuery('.countertop-request-form-open').click(function(){
            console.log('trigger');
            jQuery('.request-quote-form a.uabb-button').trigger('click');
            var getProductTitle = jQuery(this).siblings('h2.countertop_product_title').html();
            jQuery('.product-interest-field input').val(getProductTitle.replace(/\s+/g, " "));
            jQuery('.product-interest-field input').attr('value', getProductTitle.replace(/\s+/g, " ") );
            jQuery('.product-interest-field input').attr('readonly', true);
        });
     });
})(jQuery);