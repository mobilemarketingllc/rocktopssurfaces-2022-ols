<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );


add_action( 'wp_enqueue_scripts', function(){
    // wp_enqueue_script("cdn_jquery","https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1); 
	
	wp_enqueue_style('mooZoom-image-hover css', get_stylesheet_directory_uri() . '/magnify-image-hover/css/glassstyle.min.css', array(), '0.1.0', 'all');
	wp_enqueue_script("mooZoom-image-hover",get_stylesheet_directory_uri()."/magnify-image-hover/js/jQuery-mooZoom-1.0.0.js","","",1);    
	wp_enqueue_script("Jquery Zoom-image",get_stylesheet_directory_uri()."/magnify-image-hover/js/jquery.elevatezoom.min.js","","",1);
		wp_enqueue_script("Jquery bup zoom image",get_stylesheet_directory_uri()."/magnify-image-hover/js/lightzoom.min.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);    
});

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.@$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );



function my_bb_custom_fonts ( $system_fonts ) {

  $system_fonts[ 'Vela Sans ExtLt' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '200',
    ),
  );

  $system_fonts[ 'Vela Sans' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '400',
    ),
  );
  
  $system_fonts[ 'Vela Sans Med' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '500',
    ),
  );	
 	
  $system_fonts[ 'Vela Sans Bd' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '700',
    ),
  );	
	
   $system_fonts[ 'Vela Sans ExtBd' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '900',
    ),
  );	



    return $system_fonts;
}

//Add to Beaver Builder Theme Customizer
add_filter( 'fl_theme_system_fonts', 'my_bb_custom_fonts' );

//Add to Beaver Builder modules
add_filter( 'fl_builder_font_families_system', 'my_bb_custom_fonts' );

// if (! wp_next_scheduled ( 'sync_countertop_event')) {

//   $interval =  2635200;    
//   wp_schedule_event( time() + $interval , 'monthly', 'sync_countertop_event');
// }
// add_action( 'sync_countertop_event', 'sync_countertop', 10, 2 );

// function sync_countertop(){
//   global $wpdb;
//   $upload = wp_upload_dir();
//   $upload_dir = $upload['basedir'];
//   $upload_dir = $upload_dir . '/sfn-data';  
//   $table_posts = $wpdb->prefix.'posts';
//   $table_meta = $wpdb->prefix.'postmeta';	

//   write_log('Countertop sync started');

//   $permfile = $upload_dir.'/special_order_rocktop2.json';
//   $strJsonFileContents = file_get_contents($permfile);
//   $handle = json_decode($strJsonFileContents, true); 
  

//   if (empty($handle)) {
//     switch (json_last_error()) {
//       case JSON_ERROR_NONE:
//         write_log("No errors");
//         break;
//       case JSON_ERROR_DEPTH:
//                 write_log( "Maximum stack depth exceeded");
//         break;
//       case JSON_ERROR_STATE_MISMATCH:
//                 write_log( "Invalid or malformed JSON");
//         break;
//       case JSON_ERROR_CTRL_CHAR:
//                 write_log( "Control character error");
//         break;
//       case JSON_ERROR_SYNTAX:
//                 write_log( "Syntax error");
//         break;
//       case JSON_ERROR_UTF8:
//                 write_log( "Malformed UTF-8 characters");
//         break;
//       default:
//            write_log( "Unknown error");
//         break;
//     }	
//   }else{   
//     // $wpdb->query('DELETE p, pm  FROM '.$table_posts.' p INNER JOIN '.$table_meta.' pm ON pm.post_id = p.ID WHERE p.post_type = "countertop"');
//     set_time_limit(0);
//     if($handle) {
//     $k = 1;				
//       foreach( $handle  as $row) {

//         write_log('current loop ->' .$k);
                
//         if(trim(@$row['IMAGE'])!="" ){
    
//           $data['flag'] = @$row['AVAILABLITY'] ;	
//           $data['brand'] = @$row['BRAND'] ;
//           $data['product_image'] =  @$row['IMAGE']  ;
//           $data['type'] = $row['MATERIAL TYPE'] ;
//           $data['product'] = $row['SURFACE COLOR'] ;
//           $data['color'] = $row['SURFACE COLOR'];	
//           $data['sku'] = $row['SKU'];	

//           $my_post = array(
//             'post_title'    => $data['color']." ".$data['type'],
//             'post_content'  => '',
//             'post_type'  => "countertop",
//             'post_status'   => 'publish',
//             'post_author'   => 1,	
//             'meta_input'   => $data,
    
//           );		
                  
//           $wpdb->query('DELETE FROM '.$table_redirect.' WHERE title = "'.$data['brand'].' '.$data['product']." ".$data['Color'].'"');
//           $wpdb->query('DELETE FROM '.$table_redirect.' WHERE url LIKE "%'.$data['brand'].' '.$data['product']." ".$data['Color'].'%"');							
    
                                      
//           // Insert the post into the database.
//           $post_id = wp_insert_post( $my_post );
//         }
          
//         if($k % 100==0){ sleep(3);}

//         $k++;
        
//       }	
//     }
//     write_log('Countertop sync ended');
//   }
// }

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_countertop' );

function wpse_100012_override_yoast_breadcrumb_trail_countertop( $links ) {
    global $post;

    if( is_singular( 'countertop' )  ) {
      $flag = get_field('flag',$post->ID);
      if($flag == 'SPECIAL ORDER STONE'){
        $breadcrumb[] = array(
            'url' => get_site_url().'/countertops/',
            'text' => 'COUNTERTOPS',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/countertops/special-order-stone-countertops/',
            'text' => 'SPECIAL ORDER STONE COUNTERTOPS',
        );
        
        
      }else{
        $breadcrumb[] = array(
          'url' => get_site_url().'/countertops/',
          'text' => 'COUNTERTOPS',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/countertops/in-stock-countertops/',
            'text' => 'IN-STOCK COUNTERTOPS',
        );
      
      }
      array_splice( $links, 1, -1, $breadcrumb );
    }

    return $links;
}


//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;
    $instock = get_post_meta( $post->ID , "in_stock");

    if (is_singular( 'luxury_vinyl_tile' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-waterproof-luxury-vinyl-products/',
                'text' => 'In Stock Waterproof Luxury Vinyl Products',
            );
        }else{    
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/vinyl/',
                'text' => 'Waterproof Luxury Vinyl',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/vinyl/products/',
                'text' => 'Waterproof Luxury Vinyl Products',
            );
        }
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}

add_filter( 'gform_field_validation_22_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_31_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_32_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_33_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_35_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_36_20', 'custom_validation', 20, 4 );

function custom_validation( $result, $value, $form, $field ) {
	if ( $result['is_valid'] ) {
    $acceptable_zips = array(
      '84095','84118','84119','84120','84020','84065','84121','84123','84084','84088','84106','84116',
      '84107','84047','84094','84092','84070','84115','84104','84044','84128','84117','84109','84093',
      '84124','84105','84108','84103','84102','84111','84101','84112','84006','84144','84113','84180',
      '84627','84647','84642','84634','84629','84622','84646','84662','84632','84630','84665','84643',
      '84623','84667','84621','84098','84060','84036','84017','84055','84033','84061','84024','84043',
      '84003','84062','84604','84660','84058','84057','84606','84601','84663','84651','84097','84655',
      '84042','84004','84664','84653','84013','84633','84626','84032','84049','84082','84096','84005',
      '84045','84059','84526','84645','84602','84628','84603','84605'
    );

    // $zip_value = rgar( $value, $field->id . '20' );
// var_dump($value,$zip_value, $acceptable_zips);
    if ( !in_array( $value, $acceptable_zips ) ) {
        $result['is_valid'] = false;
        $result['message']  = 'Entered Zip is outside of the area, please contact us.';
    }
}

return $result;
}


add_filter( 'gform_field_validation_24_20', 'custom_validation_all', 10, 4 );
function custom_validation_all( $result, $value, $form, $field ) {
	if ( $result['is_valid'] ) {
    $acceptable_zips = array(
      '84095','84118','84119','84120','84020','84065','84121','84123','84084','84088','84106','84116',
      '84107','84047','84094','84092','84070','84115','84104','84044','84128','84117','84109','84093',
      '84124','84105','84108','84103','84102','84111','84101','84112','84006','84144','84113','84180',
      '84627','84647','84642','84634','84629','84622','84646','84662','84632','84630','84665','84643',
      '84623','84667','84621','84098','84060','84036','84017','84055','84033','84061','84024','84043',
      '84003','84062','84604','84660','84058','84057','84606','84601','84663','84651','84097','84655',
      '84042','84004','84664','84653','84013','84633','84626','84032','84049','84082','84096','84005',
      '84045','84059','84526','84645','84602','84628','84603','84605'
    );

    // $zip_value = rgar( $value, $field->id . '20' );
// var_dump($value,$zip_value, $acceptable_zips);
    if ( !in_array( $value, $acceptable_zips ) ) {
        $result['is_valid'] = false;
        $result['message']  = 'Entered Zip is outside of the area, please contact us.';
    }
}

return $result;
}


// add_filter( 'gform_field_value_sample_selection', 'my_custom_population_function' );
// function my_custom_population_function( $value ) {
//   if(isset($_GET['sid'])){
//     $samples = array(
//       "the_point_collection"=>"The Point Collection",
//       "canyon_ridge_collection"=>"Canyon Ridge Collection",
//       "mountain_crest_collection"=>"Mountain Crest Collection",
//       "powder_mountain_collection"=>"Powder Mountain Collection",
//       "cage_creek_collection"=>"Sage Creek Collection"
//     );
//     return $samples[$_GET['sid']];
//   }
    
// }
add_filter( 'gform_field_value_sample_name', 'sample_name_population_function' );
function sample_name_population_function( $value ) {
  global $wp;
  
  if(isset($_GET['sid'])){
    $samples = array(
      "the_point_collection"=>"The Point Collection",
      "canyon_ridge_collection"=>"Canyon Ridge Collection",
      "mountain_crest_collection"=>"Mountain Crest Collection",
      "powder_mountain_collection"=>"Powder Mountain Collection",
      "cage_creek_collection"=>"Sage Creek Collection"
    );
  
    $samp = $samples[$_GET['sid']] ;

  }else{

    $samples_url = array(
      "the-point-collection"=>"The Point Collection",
      "canyon-ridge-collection"=>"Canyon Ridge Collection",
      "mountain-crest-collection"=>"Mountain Crest Collection",
      "powder-mountain-collection"=>"Powder Mountain Collection",
      "sage-creek-collection"=>"Sage Creek Collection"
    );

    $queryvar =  $wp->query_vars;   
    $samp = $samples_url[$queryvar['pagename']] ;   

  }

  return $samp;
    
}

// Replace 7 with the ID of your form and 13 with the ID of the field you want to force "all required"
// http://www.gravityhelp.com/documentation/page/Gform_field_validation
// add_filter("gform_field_validation_22_3", 'validate_tcs', 10, 4);

// function validate_tcs($result, $value, $form, $field) {   
// 		// Validate the value
//     if ( $field->get_input_type() === 'email' && $result['is_valid'] ) {
// 			$result["is_valid"] = false;
// 			$result["message"] = "This field requires a unique entry and OR <a href='/contact-us/'>Contact us</a> for more information";
// 		}

	
// 	return $result;
// }

add_filter( 'gform_field_validation_22_3', 'product_quantity_validation', 10, 4 );
// add_filter( 'gform_field_validation_31_3', 'product_quantity_validation', 10, 4 );
// add_filter( 'gform_field_validation_32_3', 'product_quantity_validation', 10, 4 );
// add_filter( 'gform_field_validation_33_3', 'product_quantity_validation', 10, 4 );
// add_filter( 'gform_field_validation_35_3', 'product_quantity_validation', 10, 4 );
// add_filter( 'gform_field_validation_36_3', 'product_quantity_validation', 10, 4 );

function product_quantity_validation( $result, $value, $form, $field ) {
   if($value =='' )
   {
    $result['message']  = "This field is required.";
   }
   else 
   {
        if(filter_var($value, FILTER_VALIDATE_EMAIL)) {

            $result['message']  = "This field requires a unique entry and '".$value."' has already been used </br> <a href='/contact-us/'>Contact us</a> for more information";
        } 
        else
        {
            $result['message']  = "The email address entered is invalid, please check the formatting (e.g. email@domain.com).";
        }  
   }
    return $result;
}


add_action( 'gform_after_submission_22', 'post_to_shipsation', 10, 2 );
// add_action( 'gform_after_submission_31', 'post_to_shipsation', 10, 2 );
// add_action( 'gform_after_submission_32', 'post_to_shipsation', 10, 2 );
// add_action( 'gform_after_submission_33', 'post_to_shipsation', 10, 2 );
// add_action( 'gform_after_submission_35', 'post_to_shipsation', 10, 2 );
// add_action( 'gform_after_submission_36', 'post_to_shipsation', 10, 2 );

//shipsation intengration
function post_to_shipsation( $entry, $form ) {

  write_log('shipstation api function');

  //To fetch input inserted into your gravity form//
  $email  = rgpost( 'input_3' );
  $fname  = rgpost( 'input_1' );
  $lname  = rgpost( 'input_2' );
  $name = $fname.' '.$lname;
  $phone  = rgpost( 'input_4' );
  $add  = rgpost( 'input_17' );
  $city  = rgpost( 'input_18' );
  $state  = rgpost( 'input_19' );
  $zip  = rgpost( 'input_20' );

  if(rgpost( 'input_22' ) != ''){

    $sample = rgpost( 'input_22' );

  }else{

    $sample = rgpost( 'input_26' );
  }
  
  
  $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://ssapi.shipstation.com/orders/createorder',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "orderNumber":"'.mt_rand(100000,999999).'",   
        "orderDate":"'.date("Y-m-d").'",   
        "orderStatus":"awaiting_shipment",   
        "customerEmail":"'.$email.'",
        "billTo":{
            "name":"'.$name.'",
            "company":null,
            "street1":null,
            "street2":null,
            "street3":null,
            "city":null,
            "state":null,
            "postalCode":null,
            "country":null,
            "phone":null,
            "residential":null
        },
        "shipTo":{
            "name":"'.$name.'",
            "company":"",
            "street1":"'.$add.'",
            "street2":"null",
            "street3":null,
            "city":"'.$city.'",
            "state":"'.$state.'",
            "postalCode":"'.$zip.'",
            "country":"US",
            "phone":"'.$phone.'",
            "residential":true
        },   
        "customerNotes":"'.$sample.'"   
      }',
        CURLOPT_HTTPHEADER => array(
          'Host: ssapi.shipstation.com',
          'Content-Type: application/json',
          'Authorization: Basic ODVkNzQzYzViM2NiNDQxN2IzNjlhMWI1NjE5MzEwNzQ6MjNlYzI1MjNhZWU3NDYyNjhkZDQ3MWRhZDNjNDg5NjA='
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      write_log($response);
  }


  //shipsation intengration
function post_to_shipsation_all( $entry, $form ) {

  write_log('shipstation api function');

  //To fetch input inserted into your gravity form//
  $email  = rgpost( 'input_3' );
  $fname  = rgpost( 'input_1' );
  $lname  = rgpost( 'input_2' );
  $name = $fname.' '.$lname;
  $phone  = rgpost( 'input_4' );
  $add  = rgpost( 'input_17' );
  $city  = rgpost( 'input_18' );
  $state  = rgpost( 'input_19' );
  $zip  = rgpost( 'input_20' );
  $sample = rgpost( 'input_24' );
  
  $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://ssapi.shipstation.com/orders/createorder',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "orderNumber":"'.mt_rand(100000,999999).'",   
        "orderDate":"'.date("Y-m-d").'",   
        "orderStatus":"awaiting_shipment",   
        "customerEmail":"'.$email.'",
        "billTo":{
            "name":"'.$name.'",
            "company":null,
            "street1":null,
            "street2":null,
            "street3":null,
            "city":null,
            "state":null,
            "postalCode":null,
            "country":null,
            "phone":null,
            "residential":null
        },
        "shipTo":{
            "name":"'.$name.'",
            "company":"",
            "street1":"'.$add.'",
            "street2":"null",
            "street3":null,
            "city":"'.$city.'",
            "state":"'.$state.'",
            "postalCode":"'.$zip.'",
            "country":"US",
            "phone":"'.$phone.'",
            "residential":true
        },   
        "customerNotes":"'.$sample.'"   
      }',
        CURLOPT_HTTPHEADER => array(
          'Host: ssapi.shipstation.com',
          'Content-Type: application/json',
          'Authorization: Basic ODVkNzQzYzViM2NiNDQxN2IzNjlhMWI1NjE5MzEwNzQ6MjNlYzI1MjNhZWU3NDYyNjhkZDQ3MWRhZDNjNDg5NjA='
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      write_log($response);
  }
  add_action( 'gform_after_submission_24', 'post_to_shipsation_all', 10, 2 );



add_filter('fl_builder_module_attributes', function ($attrs, $row) {
  if ('k2gcrpz3me5n' == $row->settings->id) {
    $attrs['data-img'] = 'https://rocktopssurfaces.com/wp-content/uploads/2022/09/Untitled-1.jpg';
  }
  return $attrs;
}, 10, 2);


add_filter( 'gform_field_value_countertopsample', 'countertopsample_population_function' );
function countertopsample_population_function( $value ) {
  if (isset($_GET['product_id']) && $_GET['product_id']!='') {
   
    return get_the_title($_GET['product_id']);
  
  }
 
}

  //shipsation intengration for instock countertop
  function post_to_shipsation_countertop( $entry, $form ) {

    write_log('shipstation api function');
  
    //To fetch input inserted into your gravity form//
    $email  = rgpost( 'input_3' );
    $fname  = rgpost( 'input_1' );
    $lname  = rgpost( 'input_2' );
    $name = $fname.' '.$lname;
    $phone  = rgpost( 'input_4' );
    $add  = rgpost( 'input_17' );
    $city  = rgpost( 'input_18' );
    $state  = rgpost( 'input_19' );
    $zip  = rgpost( 'input_20' );
    $sample = rgpost( 'input_21' );
    
    $curl = curl_init();
  
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://ssapi.shipstation.com/orders/createorder',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
          "orderNumber":"'.mt_rand(100000,999999).'",   
          "orderDate":"'.date("Y-m-d").'",   
          "orderStatus":"awaiting_shipment",   
          "customerEmail":"'.$email.'",
          "billTo":{
              "name":"'.$name.'",
              "company":null,
              "street1":null,
              "street2":null,
              "street3":null,
              "city":null,
              "state":null,
              "postalCode":null,
              "country":null,
              "phone":null,
              "residential":null
          },
          "shipTo":{
              "name":"'.$name.'",
              "company":"",
              "street1":"'.$add.'",
              "street2":"null",
              "street3":null,
              "city":"'.$city.'",
              "state":"'.$state.'",
              "postalCode":"'.$zip.'",
              "country":"US",
              "phone":"'.$phone.'",
              "residential":true
          },   
          "customerNotes":"'.$sample.'"   
        }',
          CURLOPT_HTTPHEADER => array(
            'Host: ssapi.shipstation.com',
            'Content-Type: application/json',
            'Authorization: Basic ODVkNzQzYzViM2NiNDQxN2IzNjlhMWI1NjE5MzEwNzQ6MjNlYzI1MjNhZWU3NDYyNjhkZDQ3MWRhZDNjNDg5NjA='
          ),
        ));
  
        $response = curl_exec($curl);
  
        curl_close($curl);
        write_log($response);
    }
    add_action( 'gform_after_submission_38', 'post_to_shipsation_countertop', 10, 2 );


// instock_countertop_image

add_shortcode('instock_countertop_image', 'instock_countertop_image_function');

function instock_countertop_image_function($arg)
{
  if (isset($_GET['product_id']) && $_GET['product_id']!='') {
   
    $counter =  get_field('product_image',$_GET['product_id']);

    if (!preg_match("~^(?:f|ht)tps?://~i", $counter)) {
      // Add the HTTP protocol to the URL
      $counter = "https://" . $counter;
  }

    $title_counter = get_the_title($_GET['product_id']);

    return '<img src="'.$counter.'" alt="'.$title_counter.'" />';
  
  }

}